/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto01;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 *
 * @author jorge
 */
public class Ventana {

    static private JPanel panelNorth, panelSouth, panelCenter, panelWest, panelEast;
    static private ActionListener listener;
    static private JLabel texto1;
    static private JFrame ventana, ventanaDos;

    static private void panelNorth() {
        panelNorth = new JPanel();
        JButton boton1 = new JButton("Esconder");
        boton1.addActionListener(listener);
        panelNorth.setLayout(new GridLayout(1, 1));
        panelNorth.add(boton1);
    }

    static private void panelSouth() {
        panelSouth = new JPanel();
        panelSouth.setLayout(new GridLayout(1, 1));
        JButton boton2 = new JButton("Salir");
        boton2.addActionListener(listener);

        //panelSouth.add(boton2);
    }

    static private void panelCenter() {
        panelCenter = new JPanel();
        panelCenter.setLayout(new GridBagLayout());
        
        JScrollPane scrollpanel = new javax.swing.JScrollPane();
        JTable table = new JTable();

        
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1","Juanito", "Arcoiris"},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Código", "Nombre", "Apellido"
            }
        ));
        
        texto1 = new JLabel("Programar en java es fácil");
        texto1.setHorizontalAlignment(JTextField.CENTER);
        scrollpanel.setViewportView(table);
        panelCenter.add(scrollpanel);
        //panelCenter.add(texto1);
    }

    static private void panelWest() {
        panelWest = new JPanel();
    }

    static private void panelEast() {
        panelEast = new JPanel();
        panelEast.setLayout(new GridLayout(6, 1));
        JButton button1 = new JButton("Agregar espacio");
        JButton button2 = new JButton("Listar espacio");
        JButton button3 = new JButton("Agregar Persona");
        JButton button4 = new JButton("Listar persona");
        JButton button5 = new JButton("Registro Ingreso salida");
        JButton button6 = new JButton("Consultar registro");
        panelEast.add(button1);
        panelEast.add(button2);
        panelEast.add(button3);
        panelEast.add(button4);
        panelEast.add(button5);
        panelEast.add(button6);
        
    }

    static private void crearListener() {
        listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton boton = (JButton) e.getSource();

                switch (boton.getText()) {
                    case "Salir":
                        System.exit(0);
                        break;

                    case "Mostrar":
                        boton.setText("Esconder");
                        texto1.setVisible(true);
                        ventanaDos.setVisible(true);
                        JOptionPane.showMessageDialog(null, "Hola");
                        break;

                    case "Esconder":
                        boton.setText("Mostrar");
                        texto1.setVisible(false);
                        ventanaDos.setVisible(false);
                        break;
                }
                System.out.println("Hice click" + boton.getText());
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
    }

    static public void crearVentanaDos() {
        ventanaDos = new JFrame("Coso 1");
        ventanaDos.setLayout(new FlowLayout());
        ventanaDos.setVisible(true);
        ventanaDos.setMinimumSize(new Dimension(200, 200));
        ventanaDos.pack();
        
        JRadioButton button1 = new JRadioButton("Opción 1");
        JRadioButton button2 = new JRadioButton("Opcion 2");
        
        
        
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("......");
                button2.setSelected(false);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button1.setSelected(false);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        ventanaDos.add(button1);
        ventanaDos.add(button2);
    }

    static public void crearVentana() {
        ventana = new JFrame();
        ventana.setLayout(new BorderLayout(10, 0));

        crearVentanaDos();
        
        crearListener();

        panelCenter();
        panelEast();
        panelNorth();
        panelSouth();
        panelWest();

        ventana.add(panelCenter, BorderLayout.CENTER);
        ventana.add(panelEast, BorderLayout.EAST);
        ventana.add(panelNorth, BorderLayout.NORTH);
        ventana.add(panelSouth, BorderLayout.SOUTH);
        ventana.add(panelWest, BorderLayout.WEST);

        ventana.setVisible(true);
        ventana.setMinimumSize(new Dimension(500, 400));
        ventana.pack();
        ventana.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowClosed(WindowEvent e) {
                System.exit(0);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowIconified(WindowEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowActivated(WindowEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

}
