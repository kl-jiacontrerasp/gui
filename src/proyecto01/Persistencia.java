/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto01;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorge
 */
public class Persistencia {

    public static void EscribirArchivo(String nombreArchivo) {
        String[] datos = {"A", "B", "C", "D"};
        try {
            FileWriter archivo = new FileWriter(nombreArchivo);
            for (String dato : datos) {
                archivo.write(dato + "\n");
            }
            archivo.close();
        } catch (IOException ex) {
            Logger.getLogger(Persistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void AgregarContenidoArchivo(String nombreArchivo, String contenido) {

        try {
            FileWriter archivo = new FileWriter(nombreArchivo);
            archivo = new FileWriter(nombreArchivo, true);
            archivo.write(contenido + "\n");
            archivo.close();
        } catch (IOException ex) {
            Logger.getLogger(Persistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void LecturaArchivo(String nombreArchivo) {
        File archivo = new File(nombreArchivo);
        try {
            Scanner sc = new Scanner(archivo);
            while (sc.hasNextLine()) {
                String linea = sc.nextLine();
                System.out.println(linea);
            }

        } catch (IOException ex) {
            Logger.getLogger(Persistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void EscrituraObjeto(String nombreArchivo, Object obj, boolean keep) {

        ArrayList<Object> list = new ArrayList<>();
        if (keep) {
            list = LecturaObjeto(nombreArchivo);
        }

        list.add(obj);

        try {
            FileOutputStream fileOut = new FileOutputStream(nombreArchivo);
            ObjectOutputStream salida = new ObjectOutputStream(fileOut);
            for (Object object : list) {
                salida.writeObject(object);
            }
            salida.close();

        } catch (IOException ex) {
            Logger.getLogger(Persistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<Object> LecturaObjeto(String nombreArchivo) {
        FileInputStream fileIn;

        ArrayList<Object> list = new ArrayList<>();

        try {
            fileIn = new FileInputStream(nombreArchivo);
            ObjectInputStream entrada = new ObjectInputStream(fileIn);

            Object object = new Object();

            while (object != null) {
                object = entrada.readObject();
                if (object != null) {
                    list.add(object);
                }
            }

        } catch (IOException ex) {
            //Logger.getLogger(Persistencia.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            //Logger.getLogger(Persistencia.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return list;
        }
    }
}
