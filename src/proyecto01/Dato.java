/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto01;

import java.io.Serializable;

/**
 *
 * @author jorge
 */
public class Dato implements Serializable{
    private String id;
    private String data;

    public Dato() {
    }

    
    
    public Dato(String id, String data) {
        this.id = id;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Dato{" + "id=" + id + ", data=" + data + '}';
    }
    
    
    
}
